package com.example.emillozev.radiostream.ReceiveMetaData;

import com.example.emillozev.radiostream.R;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class StreamMeta {
    protected URL mStreamURL;
    private Map<String, String> mMetadata;
    private String mStationName;
    private String mStationGenre;
    private String mWebSiteUrl;

    public StreamMeta(URL streamURL) throws IOException {
        mStationName = "";
        mStationGenre = "";
        mWebSiteUrl = "";
        setStreamURL(streamURL);
    }

    public String getWebSiteUrl() {
        return mWebSiteUrl;
    }

    public String getStationGenre() {
        return mStationGenre;
    }

    public String getStationName() {
        return mStationName;
    }

    private void setStreamURL(URL streamURL) {
        this.mMetadata = null;
        this.mStreamURL = streamURL;
    }

    /**
     * Getting the artist from the metadata
     * @return
     * @throws IOException
     */
    public String getArtist() throws IOException {
        Map<String, String> data = getMetaData();
        if (!data.containsKey("StreamTitle"))
            return "";

        String streamTitle = data.get("StreamTitle");
        int pos = streamTitle.indexOf("-");
        String title = (pos == -1) ? streamTitle : streamTitle.substring(0, pos);

        return title.trim();
    }

    /**
     * Getting the title of the stream
     * @return
     * @throws IOException
     */
    public String getTitle() throws IOException {
        Map<String, String> data = getMetaData();

        if (!data.containsKey("StreamTitle"))
            return "";

        String streamTitle = data.get("StreamTitle");
        String artist = streamTitle.substring(streamTitle.indexOf("-") + 1);
        return artist.trim();
    }

    /**
     * Checking if metadata is null, if so retrieves new one
     * @return Map of Artist and Song
     * @throws IOException thrown by receiveMetaData()
     */
    public Map<String, String> getMetaData() throws IOException {
        if (mMetadata == null) {
            receiveMetaData();
        }
        return mMetadata;
    }

    /**
     * This method is getting all the metadata
     * @throws IOException - when it is not possible to open the connection
     */
    public void receiveMetaData() throws IOException {
        URLConnection connection = mStreamURL.openConnection();
        connection.setRequestProperty("Icy-metadata", "1");//Icy-metadata
        connection.setRequestProperty("Connection", "close");
        connection.setRequestProperty("Accept", null);

        int metaDataOffset = 0;
        Map<String, List<String>> headers = connection.getHeaderFields();
        InputStream inputStream = connection.getInputStream();

        mStationName = headers.get("icy-name").get(0);
        mStationGenre = headers.get("icy-genre").get(0);
        mWebSiteUrl = headers.get("icy-url").get(0);


        if (headers.containsKey("icy-metaint")) {
            metaDataOffset = Integer.parseInt(headers.get("icy-metaint").get(0));
        } else {
            StringBuilder stringHeaders = new StringBuilder();
            char c;

            while ((c = (char) inputStream.read()) != -1) {
                stringHeaders.append(c);
                if (stringHeaders.length() > 5 && (stringHeaders.substring((stringHeaders.length() - 4), stringHeaders.length()).equals("\r\n\r\n"))) {
                    break;
                }
            }

            Pattern pattern = Pattern.compile(String.valueOf(R.string.icy_metaint_pattern));
            Matcher matcher = pattern.matcher(stringHeaders.toString());

            if (matcher.find()) {
                metaDataOffset = Integer.parseInt(matcher.group(2));
            }
        }

        if (metaDataOffset == 0) {
            return;
        }

        int b, count = 0;
        int metaDataLength = 4080;
        boolean inData = false;

        StringBuilder metaData = new StringBuilder();

        while ((b = inputStream.read()) != -1) {
            count++;

            // Length of the metadata
            if (count == metaDataOffset + 1) {
                metaDataLength = b * 16;
            }

            if (count > metaDataOffset + 1 && count < (metaDataOffset + metaDataLength)) {
                inData = true;
            } else {
                inData = false;
            }
            if (inData) {
                if (b != 0) {
                    metaData.append((char) b);
                }
            }
            if (count > (metaDataOffset + metaDataLength)) {
                break;
            }

        }

        // Set the data
        mMetadata = StreamMeta.parseMetadata(metaData.toString());

        // Close
        inputStream.close();
    }

    /**
     * Metadata is being converted to comprehensible data
     * @param metaString
     * @return Map which contains Artist and Song currently playing
     */
    public static Map<String, String> parseMetadata(String metaString) {
        Map<String, String> metadata = new HashMap();
        String[] metaParts = metaString.split(";");
        Pattern p = Pattern.compile("^([a-zA-Z]+)=\\'([^\\']*)\\'$");
        Matcher m;
        for (int i = 0; i < metaParts.length; i++) {
            m = p.matcher(metaParts[i]);
            if (m.find()) {
                metadata.put(m.group(1), m.group(2));
            }
        }

        return metadata;
    }


}







