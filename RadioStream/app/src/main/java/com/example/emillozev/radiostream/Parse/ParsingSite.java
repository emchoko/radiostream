package com.example.emillozev.radiostream.Parse;

import android.util.Log;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class ParsingSite {
    private Map<String, String> mountPoints;

    /**
     * Parsing site and getting all necessary links for the stream
     * @param siteURL
     * @return - returns Map with ordered keys from 1 to size of all streams and values that are the links
     */
    public Map<String, String> parsingTheSite(final String siteURL) {

        mountPoints = new HashMap<>();

        Thread downloadThread = new Thread(new Runnable() {

            public void run() {
                org.jsoup.nodes.Document doc = null;

                try {
                    doc = Jsoup.connect(siteURL).get();
                } catch (IOException e) {
                    return;
                }

                Elements newsHeadlines = doc.select("a");
                int counter = 1;

                for (Element link : newsHeadlines) {
                    String streamURL = link.attr("href");

                    String[] extension = streamURL.split("\\.");

                    if (extension.length > 1) {
                        if (extension[extension.length - 1].equals("m3u")) {
                            String finalExtension = "";
                            for (int i = 0; i < extension.length - 1; i++) {
                                finalExtension += extension[i];
                            }
                            mountPoints.put(counter + "", "http://streaming.swisstxt.ch:80" + finalExtension);
                            Log.i("MOUNTPOINTS", counter + ": " + "http://streaming.swisstxt.ch:80" + finalExtension);
                            counter++;
                        }
                    }

                }
            }
        });
        downloadThread.start();
        try {
            downloadThread.join();
        } catch (InterruptedException e) {
            Log.i("HREF", "FAIL");
        }

        return mountPoints;
    }

}
