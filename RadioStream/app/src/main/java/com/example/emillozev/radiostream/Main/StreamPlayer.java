package com.example.emillozev.radiostream.Main;

import android.annotation.TargetApi;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.emillozev.radiostream.BackgroundService.PlaybackService;
import com.example.emillozev.radiostream.Parse.ParsingSite;
import com.example.emillozev.radiostream.R;
import com.example.emillozev.radiostream.ReceiveMetaData.StreamMeta;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CopyOnWriteArrayList;

public class StreamPlayer extends AppCompatActivity {

    public static final int DELAY_MILLIS = 15000;
    public static final int PERIOD_REFRESH_META = 10000;
    private Button mPlayPauseButton;
    private String Stream_URL = "http://streaming.swisstxt.ch:80/s/regi_ag_so/aacp_32";
    public static ProgressBar mSpinner;
    public static TextView mSpinnerText;
    private ListView mStationList;
    private TextView mCurrentRadioStationName;
    private TextView mCurrentSongPlaying;
    private View mFooter;
    private TextView mRadioGenre;
    private TextView mRadioWebSite;
    private int mListPageCount = 1;
    private CopyOnWriteArrayList<String> returnList;
    private List<String> listStations;
    private int mStart = 0;
    private int mStop = 15;
    private ArrayAdapter<String> mArrayAdapter;
    private Intent mPlayIntent;
    private static final String mSiteURL = "http://livestream.srg-ssr.ch/status.xsl";

    private PlaybackService mMusicService = new PlaybackService(Stream_URL);

    private ServiceConnection mMusicConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            PlaybackService.MusicBinder musicBinder = (PlaybackService.MusicBinder) iBinder;
            mMusicService = musicBinder.getService();
            mMusicService.setURL(Stream_URL);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
        }
    };

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stream_player);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initializeViews();

        Map<String, String> mRadioStations;

        setUpSpinnerAndButton();
        mPlayIntent = new Intent(this, PlaybackService.class);

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                mCurrentRadioStationName.setText(getMetaName(Stream_URL));
            }
        });
        thread.start();
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        mRadioWebSite.setText(getGenreAndWebsite(Stream_URL).get(0));
        mRadioGenre.setText(getGenreAndWebsite(Stream_URL).get(1));


        ParsingSite parse = new ParsingSite();
        mRadioStations = new HashMap<>(parse.parsingTheSite(mSiteURL));

        listStations = new ArrayList<>(mRadioStations.values());

        returnList = new CopyOnWriteArrayList<>();

        MetadataDownloader downloader = new MetadataDownloader();
        downloader.execute();

        setUpListViewAndListeners();

        getMeta(Stream_URL);


        if (mPlayIntent == null) {
            mPlayIntent = new Intent(this, PlaybackService.class);
            bindService(mPlayIntent, mMusicConnection, Context.BIND_AUTO_CREATE);
            startService(mPlayIntent);
            mMusicService.setURL(Stream_URL);
        }

        mMusicService.setURL(Stream_URL);
    }

    /**
     * Starting the playback background service from here
     */
    @Override
    protected void onStart() {
        super.onStart();
        mPlayIntent = new Intent(this, PlaybackService.class);
        bindService(mPlayIntent, mMusicConnection, Context.BIND_AUTO_CREATE);
        startService(mPlayIntent);
    }

    /**
     * Initialising the list view used for the radio stations list
     * and the onScroll Listeners and onItemClickListener
     */
    private void setUpListViewAndListeners() {
        mStationList.addFooterView(mFooter);

        mArrayAdapter = new ArrayAdapter<>(this,
                R.layout.list_row,
                returnList);

        mStationList.setAdapter(mArrayAdapter);

        mStationList.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {
            }

            @Override
            public void onScroll(AbsListView absListView, int i, int i1, int i2) {
                final int totalItems = i2;

                int total = i + i1;

                if (mListPageCount < 6) {
                    if (total == totalItems) {
                        mStationList.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mStart += 15;
                                mStop += 15;
                                MetadataDownloader metadataDownloader = new MetadataDownloader();
                                metadataDownloader.execute();
                                mArrayAdapter.notifyDataSetChanged();
                                mStationList.setSelection(totalItems);
                                mListPageCount++;
                            }
                        }, DELAY_MILLIS);
                    }
                } else {
                    mStationList.removeFooterView(mFooter);
                }

            }
        });

        mStationList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int i, long l) {
                int itemPosition = i + 1;
                mCurrentSongPlaying.setText("");
                //Toast.makeText(getApplicationContext(), i, Toast.LENGTH_SHORT).show();

                changeRadioStation(listStations.get(i), i);
            }
        });

    }

    /**
     * Setting up spinner for buffering and listener for play/pause button
     */
    private void setUpSpinnerAndButton() {
        mSpinner.setVisibility(View.VISIBLE);
        setVolumeControlStream(AudioManager.STREAM_MUSIC);

        mPlayPauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mMusicService.isPlaying()) {
                    mMusicService.stopStream();
                    mPlayPauseButton.setText(R.string.play_text);
                } else {
                    mMusicService.continuePlay();
                    mPlayPauseButton.setText(R.string.pause_text);
                }
            }
        });

    }

    /**
     * Initialising views
     */
    private void initializeViews() {
        mCurrentRadioStationName = (TextView) findViewById(R.id.radioStationName);
        mStationList = (ListView) findViewById(R.id.listOfStations);
        mSpinnerText = (TextView) findViewById(R.id.progressBarText);
        mSpinner = (ProgressBar) findViewById(R.id.progressBar);
        mPlayPauseButton = (Button) findViewById(R.id.playButton);
        mCurrentSongPlaying = (TextView) findViewById(R.id.currentlyPlaying);
        mRadioGenre = (TextView) findViewById(R.id.radioGenre);
        mRadioWebSite = (TextView) findViewById(R.id.radioWebSite);
        mFooter = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.progress_bar, null, false);
    }

    /**
     * Retrieving stream metadata and getting the Genre and Website
     *
     * @param url - URL of the stream
     * @return - List with two values in it: first - WebSite; second - Genre
     */
    private List<String> getGenreAndWebsite(final String url) {
        final List<String> returnList = new ArrayList<>();

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    StreamMeta icy = new StreamMeta(new URL(url));
                    icy.getMetaData();
                    returnList.add(icy.getWebSiteUrl());
                    returnList.add(icy.getStationGenre());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        thread.start();
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return returnList;
    }

    /**
     * Changing radio station by telling PlaybackService to do so
     * and setting the appropriate
     *
     * @param url         - next radio url
     * @param radioNumber - the number of the radio in the list of all radios
     */
    private void changeRadioStation(final String url, int radioNumber) {

        List<String> genreAndWebsite = new ArrayList<>(getGenreAndWebsite(url));

        mRadioWebSite.setText(genreAndWebsite.get(0));
        mRadioGenre.setText(genreAndWebsite.get(1));
        Stream_URL = url;

        mCurrentRadioStationName.setText(returnList.get(radioNumber));
        mCurrentSongPlaying.setText("");
        mSpinnerText.setText(R.string.buffering_text);
        mSpinner.setVisibility(View.VISIBLE);

        mMusicService.changeRadioStation(url);
        mCurrentSongPlaying.setText(getMeta(Stream_URL));
    }

    /**
     * Retrieving the name of the Stream
     *
     * @param url - url of the stream
     * @return - the name of the stream
     */
    private String getMetaName(final String url) {
        try {
            StreamMeta icy = new StreamMeta(new URL(url));
            icy.getMetaData();
            return icy.getStationName();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * Retrieving information for current song playing
     *
     * @param url - url of the stream
     * @return - Artist and Song if found
     */
    private String mArtistSong;
    private String getMeta(final String url) {
        mArtistSong = "----No data found----";
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            public void run() {
                try {
                    StreamMeta icy = new StreamMeta(new URL(url));
                    mArtistSong = icy.getArtist() + " - " + icy.getTitle();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mCurrentSongPlaying.setText(mArtistSong);
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }, 0, PERIOD_REFRESH_META);
        return mArtistSong;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mMusicService.destroyStream();
        stopService(new Intent(this, PlaybackService.class));

    }

    /**
     * Getting all of stations names and putting them in a list,
     * which is used for the ListView
     */
    private String station;

    private class MetadataDownloader extends AsyncTask<Void, Void, Void> {
        @Override
        protected synchronized Void doInBackground(Void... voids) {
            for (int i = mStart; i < mStop; i++) {
                if (i < 98) {
                    //final String stationName
                    station = getMetaName(listStations.get(i));
                    Charset.forName("UTF-8").encode(station);

                    try {
                        station = new String(station.getBytes("UTF8"));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    //String stationEncoded = new String(stationName, "Windows-1252");
                    if (mStart != 0) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                returnList.add(station);
                                mArrayAdapter.notifyDataSetChanged();
                            }
                        });
                    }

                }
            }
            return null;
        }
    }


}
