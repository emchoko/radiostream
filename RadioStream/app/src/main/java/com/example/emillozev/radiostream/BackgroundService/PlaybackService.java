package com.example.emillozev.radiostream.BackgroundService;

import android.app.Service;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;

import com.example.emillozev.radiostream.Main.StreamPlayer;

import java.io.IOException;

/**
 * Tutorial used for the service. See <a href="http://code.tutsplus.com/tutorials/create-a-music-player-on-android-song-playback--mobile-22778"/>
 */
public class PlaybackService extends Service implements
        MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener,
        MediaPlayer.OnCompletionListener {

    private MediaPlayer mMediaPlayer;
    private String STREAM_URL;
    private final IBinder mMusicBind = new PlaybackService.MusicBinder();

    @Override
    public void onCreate() {
        super.onCreate();
        initializeMedia();
        Log.i("SERVICETAG", "LOGGING");
    }

    /**
     * Initialising Media Player
     */
    private void initializeMedia() {
        mMediaPlayer = new MediaPlayer();
        mMediaPlayer.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);
        mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mMediaPlayer.setOnPreparedListener(this);
        mMediaPlayer.setOnErrorListener(this);
        mMediaPlayer.setOnCompletionListener(this);
        playStream();
    }

    public PlaybackService(String url) {
        this.STREAM_URL = url;
    }

    public PlaybackService() {}

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        mMediaPlayer.start();
        StreamPlayer.mSpinner.setVisibility(View.GONE);
        StreamPlayer.mSpinnerText.setText("");
    }

    /**
     * Changing current station playing
     * @param url - next station url
     */
    public void changeRadioStation(String url) {
        mMediaPlayer.reset();
        STREAM_URL = url;
        try {
            mMediaPlayer.setDataSource(url);
        } catch (IOException e) {
            e.printStackTrace();
        }

        mMediaPlayer.prepareAsync();
    }

    /**
     * Stopping the playback and releasing all of the Media Player resources used
     */
    public void destroyStream() {
        mMediaPlayer.stop();
        mMediaPlayer.release();
    }

    /**
     * Starting the stream
     */
    public void playStream() {
        mMediaPlayer.reset();
        try {
            mMediaPlayer.setDataSource("http://streaming.swisstxt.ch:80/s/regi_ag_so/aacp_32");
        } catch (IOException e) {
            e.printStackTrace();
        }

        mMediaPlayer.prepareAsync();
    }

    public boolean isPlaying() {
        return mMediaPlayer.isPlaying();
    }

    public void stopStream() {
        mMediaPlayer.pause();
    }

    public void setURL(String url) {
        STREAM_URL = url;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mMusicBind;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        mMediaPlayer.stop();
        mMediaPlayer.release();
        return false;
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {}

    @Override
    public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
        return false;
    }

    public void continuePlay() {
        mMediaPlayer.start();
    }

    public class MusicBinder extends Binder {
        public PlaybackService getService() {
            return PlaybackService.this;
        }
    }

}